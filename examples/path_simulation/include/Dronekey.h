#ifndef DRONEKEY_GENERAL
#define DRONEKEY_GENERAL

#include <hal_quadrotor/control/Waypoint.h>
#include <list>

typedef std::list<hal_quadrotor::Waypoint> waypointList;

struct UAV_NAV {
	std::string name;
	waypointList waypoints;
};

struct sensorsPositions {
	std::string name;
	hal_quadrotor::Waypoint wp;
};

typedef std::list<UAV_NAV> uavsList;
typedef std::list< sensorsPositions > sensorsList;

#endif