#ifndef DRONEKEY_QUADCOPTER
#define DRONEKEY_QUADCOPTER

#include <Dronekey.h>
#include <ros/ros.h>

#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include <sim/Insert.h>
#include <sim/Resume.h>
#include <sim/Pause.h>
#include <sim/Delete.h>

#include <hal_quadrotor/control/Takeoff.h>
#include <hal_quadrotor/control/Land.h>
#include <hal_quadrotor/control/Waypoint.h>

namespace dronekey {
	class Quadcopter {
	private:
		bool isLanded;

		ros::Subscriber subEstimate;

		waypointList quadcopterWP;

		waypointList::iterator movement_iterator;

		void WaypointChecker(const hal_quadrotor::State::ConstPtr& msg);
	protected:
		ros::NodeHandle gRosHandle;

		std::string quadName;

		void initWaypointChecker();
	public:
		Quadcopter(ros::NodeHandle &n, std::string &modelName);

		bool getIsLanded();

		bool spawn();

		bool remove();

		bool takeoff(float altitude);

		bool land();

		void gotoWaypoint(waypointList &wps);
	};
}

#endif