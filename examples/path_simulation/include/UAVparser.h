#ifndef DRONEKEY_UAV_PARSER
#define DRONEKEY_UAV_PARSER

#include <Dronekey.h>

#include <hal_quadrotor/control/Waypoint.h>

#include <ros/ros.h>

#include <sstream>
#include <string>
#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include "boost/lexical_cast.hpp"

namespace dronekey {
	class UAVparser {
	private:
		/**
		 * JSON Parse tree
		 */
		boost::property_tree::ptree pt;
		/**
		 * Keep track of UAV IDs
		 */
		int uavID;
		/**
		 * Name to append to!!
		 */
		std::string uavName;
	public:
		UAVparser(std::string &jsonfile, std::string &UAV);

		void parse(uavsList &wp);

		void dumpSensors(uavsList &wp);
	};
}

#endif