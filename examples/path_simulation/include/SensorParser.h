#ifndef DRONEKEY_SENSOR_PARSER
#define DRONEKEY_SENSOR_PARSER

#include <Dronekey.h>
#include <hal_quadrotor/control/Waypoint.h>

#include <ros/ros.h>

#include <sstream>
#include <string>
#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include "boost/lexical_cast.hpp"

namespace dronekey {
	class SensorParser {
	private:
		boost::property_tree::ptree pt;
		int sID;
		std::string sName;
	public:
		SensorParser(std::string &jsonfile, std::string &sensorName);

		void parse(sensorsList &sl);

		void dumpSensors(sensorsList &sl);
	};
}

#endif