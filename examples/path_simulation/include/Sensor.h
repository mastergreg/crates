#ifndef DRONEKEY_SENSOR
#define DRONEKEY_SENSOR

#include <Quadcopter.h>

#include <hal_quadrotor/control/Waypoint.h>

#include <ros/ros.h>
#include <ctime>

namespace dronekey {
	class Sensor : public Quadcopter {
	private:
		waypointList position;
	public:
		Sensor(waypointList &wp, ros::NodeHandle &n, std::string &modelName);
	
		void Start();
	};
}

#endif