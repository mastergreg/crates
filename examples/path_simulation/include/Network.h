#ifndef DRONEKEY_PATH_SIMULATION
#define DRONEKEY_PATH_SIMULATION

namespace dronekey {
	class Network {
	public:
		Network();

		void send();

		void receiver();
	};
}

#endif