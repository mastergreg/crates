#include <SensorParser.h>

using namespace dronekey;

SensorParser::SensorParser(std::string &jsonfile, std::string &sensorName)
{	
	sID = 0;
	sName = sensorName;
	//Open file
	std::ifstream wpFile (jsonfile.c_str());
	std::stringstream jsonStream;
	
	//File open??
	if(wpFile){
		//Input file into buffer
		jsonStream << wpFile.rdbuf();
		//output buffer
		std::cout << jsonStream.str() << std::endl;
		boost::property_tree::read_json(jsonStream, pt);
		wpFile.close();
	}
}

void SensorParser::parse(sensorsList &sl)
{
	BOOST_FOREACH(boost::property_tree::ptree::value_type &vPath, pt.get_child("sensors"))
	{
		sensorsPositions tmp;
		tmp.name = sName + boost::lexical_cast<std::string>(sID++);
		tmp.wp.request.x = vPath.second.get<double>("x");
		tmp.wp.request.y = vPath.second.get<double>("y");
		tmp.wp.request.z = vPath.second.get<double>("z");	
		sl.push_back(tmp);
	}
}

void SensorParser::dumpSensors(sensorsList &sl)
{
	for(sensorsList::iterator iSensor = sl.begin(); iSensor != sl.end(); iSensor++){
		ROS_INFO("SENSOR: %s --X:%f-Y:%f-Z:%f--",iSensor->name.c_str(), iSensor->wp.request.x, iSensor->wp.request.y, iSensor->wp.request.z);
	}
}