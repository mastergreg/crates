#include <Quadcopter.h>

using namespace dronekey;

Quadcopter::Quadcopter(ros::NodeHandle &n, std::string &modelName) 
{
	gRosHandle = n;

	quadName = modelName;

	isLanded = false;
}

bool Quadcopter::spawn()
{
	ros::ServiceClient srvInsert = gRosHandle.serviceClient<sim::Insert>("/simulator/Insert");
	ros::ServiceClient srvResume = gRosHandle.serviceClient<sim::Resume>("/simulator/Resume");
	ros::ServiceClient srvPause = gRosHandle.serviceClient<sim::Pause>("/simulator/Pause");

	sim::Pause msgPause;
	sim::Resume msgResume;
	sim::Insert msgInsert;

	if(!srvPause.call(msgPause)){
		return false;
	}

	msgInsert.request.model_name = quadName;
	msgInsert.request.model_type = "model://hummingbird";
	if(!srvInsert.call(msgInsert)){
		return false;
	}

	if(!srvResume.call(msgResume)){
		return false;
	}

	ros::Rate rate(4);
	rate.sleep();

	return true;
}

bool Quadcopter::remove()
{
	ros::ServiceClient srvDelete = gRosHandle.serviceClient<sim::Delete>("/simulator/Delete");
	ros::ServiceClient srvResume = gRosHandle.serviceClient<sim::Resume>("/simulator/Resume");
	ros::ServiceClient srvPause = gRosHandle.serviceClient<sim::Pause>("/simulator/Pause");

	sim::Pause msgPause;
	sim::Resume msgResume;
	sim::Delete msgDelete;

	if(!srvPause.call(msgPause)){
		return false;
	}

	if(!srvDelete.call(msgDelete)){
		return false;
	}

	if(!srvResume.call(msgResume)){
		return false;
	}

	return true;
}

bool Quadcopter::takeoff(float altitude)
{
	isLanded = false;

	hal_quadrotor::Takeoff msgTakeOff;
	ros::ServiceClient srvTakeOff = gRosHandle.serviceClient<hal_quadrotor::Takeoff>("/hal/"+quadName+"/controller/Takeoff");

	msgTakeOff.request.altitude = altitude;
	if(!srvTakeOff.call(msgTakeOff)){
		ROS_INFO("COULDN'T TAKE OFF");
		return false;
	}
	ROS_INFO("TAKE OFF %s", quadName.c_str());
	return true;
}

bool Quadcopter::land()
{
	ROS_INFO("LAND - %s !!", quadName.c_str());
	hal_quadrotor::Land msgLand;
	ros::ServiceClient srvLand = gRosHandle.serviceClient<hal_quadrotor::Land>("/hal/"+quadName+"/controller/Land");

	if(!srvLand.call(msgLand)){
		return false;
	}
	return true;
}

void Quadcopter::gotoWaypoint(waypointList &wps)
{
	isLanded = false;
	//Set way points
	quadcopterWP = wps;
	//Initialize Quadcopter
	movement_iterator = quadcopterWP.begin();
	//Start Waypoint Checker!!
	initWaypointChecker();
}

void Quadcopter::initWaypointChecker()
{
	subEstimate = gRosHandle.subscribe("/hal/" + quadName + "/Estimate", 1000, &Quadcopter::WaypointChecker, this);
}

void Quadcopter::WaypointChecker(const hal_quadrotor::State::ConstPtr& msg)
{
	ROS_INFO("Quadrotor %s Update: [%f, %f, %f, %f] Destination State: [%d, %d]", quadName.c_str(), msg->x, msg->y, msg->z, msg->yaw, msg->rch, msg->ctrl);
	
	/**
	 * @todo fix first waypoint being skipped
	 */
	if(msg->ctrl == 2 || msg->rch == 1){
		ROS_INFO("GOAL REACHED!!");
		//Unregister Checker!
		if(quadcopterWP.end() == movement_iterator){
			subEstimate.shutdown();
			isLanded = true;
			land();
		} else {
			ros::ServiceClient srvWaypoint = gRosHandle.serviceClient<hal_quadrotor::Waypoint>("/hal/"+quadName+"/controller/Waypoint");
			hal_quadrotor::Waypoint& tmp = *movement_iterator;
			tmp = *movement_iterator;
			ROS_INFO("!!!--CALLING NEW WAYPOINT--!!! - NEW COORDIANTES: [%f, %f, %f]",tmp.request.x, tmp.request.y, tmp.request.z);
			if(!srvWaypoint.call(tmp)){
				ROS_FATAL("NO WAYPOINT!!");
			}
			++movement_iterator;
		}
	}
}

bool Quadcopter::getIsLanded()
{
	return isLanded;
}