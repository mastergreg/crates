#include <UAVparser.h>

using namespace dronekey;

UAVparser::UAVparser(std::string &jsonfile, std::string &UAV)
{	
	uavID = 0;
	uavName = UAV;

	//Open file
	std::ifstream wpFile (jsonfile.c_str());
	std::stringstream jsonStream;
	
	//File open??
	if(wpFile){
		//Input file into buffer
		jsonStream << wpFile.rdbuf();
		//output buffer
		std::cout << jsonStream.str() << std::endl;
		boost::property_tree::read_json(jsonStream, pt);
		wpFile.close();
	}
}

void UAVparser::parse(uavsList &wp)
{
	BOOST_FOREACH(boost::property_tree::ptree::value_type &vPath, pt.get_child("UAVs"))
	{
		UAV_NAV tmp;
		BOOST_FOREACH(boost::property_tree::ptree::value_type &uavPaths, vPath.second)
		{
			hal_quadrotor::Waypoint tmpWP;
			tmpWP.request.x = uavPaths.second.get<double>("x");
			tmpWP.request.y = uavPaths.second.get<double>("y");
			tmpWP.request.z = uavPaths.second.get<double>("z");
			tmp.waypoints.push_back(tmpWP);	
		}

		tmp.name = uavName + boost::lexical_cast<std::string>(uavID++);	
		wp.push_back(tmp);
	}
}

void UAVparser::dumpSensors(uavsList &wp)
{
	for(uavsList::iterator iUAV = wp.begin(); iUAV != wp.end(); iUAV++){
		ROS_INFO("UAV: %s",iUAV->name.c_str());
		for(waypointList::iterator iWaypoint = iUAV->waypoints.begin(); iWaypoint != iUAV->waypoints.end(); iWaypoint++){
			ROS_INFO("--X:%f-Y:%f-Z:%f--", iWaypoint->request.x, iWaypoint->request.y, iWaypoint->request.z);
		}
	}
}
