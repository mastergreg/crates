#include <Sensor.h>

using namespace dronekey;

Sensor::Sensor(waypointList &wp, ros::NodeHandle &n, std::string &modelName) : Quadcopter(n, modelName) 
{
	position = wp;
}

void Sensor::Start()
{
	ROS_INFO("SPAWNING!!");
	Quadcopter::spawn();
	ROS_INFO("TAKEOFF");
	srand(time(NULL)); 
	int alt = 3+rand()%15;
	ROS_INFO("ALTITUDE: %d", alt);
	Quadcopter::takeoff(alt);
	//Handle Waypoint
	Quadcopter::gotoWaypoint(position);
}