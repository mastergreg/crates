Sensors: 50
UAVs: 6
Path: 1 for each UAV, look into the path file, for example,
{"1path":[{"x":50,"y":50,"z":50},{"x":40,"y":92,"z":3},{"x":56,"y":70,"z":30},{"x":18,"y":66,"z":48},{"x":35,"y":42,"z":46},{"x":43,"y":20,"z":78},{"x":37,"y":33,"z":90},{"x":50,"y":50,"z":50}]}}

(50,50,50) is the starting and end point(not a sensor point) of the UAV which is not included into the sensors file.

TEST:
Communication Protocol:
when UAV arrive to the sensor point, it stops sending out the unicast requirement to sensor. once sensor receives the packet, it replied with data packet.

Every sensor sends out one packet.

The stopping time of UAV depends on the propagation speed and data packet length. 