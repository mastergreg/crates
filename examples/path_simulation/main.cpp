#include <ros/ros.h>
#include <Dronekey.h>
#include <SensorParser.h>
#include <UAVparser.h>
#include <Sensor.h>

std::map<std::string, boost::thread*> liveSensors;
std::map<std::string, dronekey::Sensor*> allSensors;

void startSensors(sensorsList &sl, ros::NodeHandle &n)
{

	for(sensorsList::iterator iSensor = sl.begin(); iSensor != sl.end();iSensor++){

			//Expects List
			waypointList tmpWPlist;
			tmpWPlist.push_back(iSensor->wp);
			
			dronekey::Sensor *tmpSensor = new dronekey::Sensor(tmpWPlist, n, iSensor->name);
			allSensors[iSensor->name] = tmpSensor;
			liveSensors[iSensor->name] = new boost::thread(boost::bind(&dronekey::Sensor::Start, tmpSensor));
			liveSensors[iSensor->name]->detach();
			boost::this_thread::sleep(boost::posix_time::seconds(200));
	}
}

int main(int argc, char **argv)
{
	if(argc < 3){
		ROS_FATAL("FILE PATHS");
		return 1;
	}

	ros::init(argc, argv, "path_simulation");

	ros::NodeHandle n;

	std::string sensorsFile = argv[1];
	std::string quadcoptersFile = argv[2];

	std::string sensorName = "SENSOR";
	std::string uavName = "UAV";

	dronekey::SensorParser sParser(sensorsFile, sensorName);
	dronekey::UAVparser uavParser(quadcoptersFile, uavName);

	sensorsList sensors;
	uavsList	UAVs;

	sParser.parse(sensors);
	sParser.dumpSensors(sensors);

	uavParser.parse(UAVs);
	uavParser.dumpSensors(UAVs);

	boost::thread startThread(startSensors, sensors, n);

	ros::spin();

	return 0;
}