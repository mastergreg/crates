#ifndef HAL_QUADROTOR_COMMUNICATION_H
#define HAL_QUADROTOR_COMMUNICATION_H

#include <dronekey_wireless/Packet.h>

#include <ros/ros.h>

namespace hal
{
	namespace quadrotor 
	{
		class Communication 
		{
		private:
			
			ros::ServiceServer srvReceive;

			bool RcvPacket(
				dronekey_wireless::Packet::Request &req,
				dronekey_wireless::Packet::Response &res);
		public:

			void Init(ros::NodeHandle nh);
		};
	}
}

#endif