#include <hal_quadrotor/Communication.h>

using namespace hal::quadrotor;

void Communication::Init(ros::NodeHandle nh)
{
	srvReceive = nh.advertiseService("wireless/Receive", &Communication::RcvPacket, this);
}	

bool Communication::RcvPacket(
	dronekey_wireless::Packet::Request &req,
	dronekey_wireless::Packet::Response &res)
{
	ROS_INFO("PACKET RECEIVED!!: IP: %s MSG: %s", req.from.c_str(), req.pkt.c_str());
	return true;
}