set config(channel)        Channel/WirelessChannel    ;# Channel Type
set config(prop)           Propagation/Shadowing      ;# radio-propagation model
set config(netif)          Phy/WirelessPhy/802_15_4
set config(mac)            Mac/802_15_4
set config(ifq)            Queue/DropTail/PriQueue    ;# interface queue type
set config(ll)             LL                         ;# link layer type
set config(ant)            Antenna/OmniAntenna        ;# antenna model
set config(ifqlen)         5                          ;# max packet in ifq
set config(num_nodes)      50                         ;# number of mobilenodes
set config(rp)             DumbAgent                  ;# routing protocol
set config(x)		50
set config(y)		50

set ns_ [new Simulator]

set tracefd [ open /tmp/simple.tr w]
$ns_ trace-all $tracefd

set namtrace     [open /tmp/simple.nam w]
$ns_ namtrace-all-wireless $namtrace $config(x) $config(y)

# inform nam that this is a trace file for wpan (special handling needed)
$ns_ puts-nam-traceall {# nam4wpan #}		;

# default = off (should be turned on before other 'wpanNam' commands can work)
Mac/802_15_4 wpanNam namStatus on		;

#Reset Antenna location
Antenna/OmniAntenna set Z_ 0.0

#Parameters determined by MATLAB
#--------simple.tcl Parameters---------
Propagation/Shadowing set std_db_ 2.31262e+00
Propagation/Shadowing set dist0_ 1.89644e+00
Propagation/Shadowing set pathlossExp_ 4.26445e-01
Phy/WirelessPhy set Pt_ 1.99526e+00
Phy/WirelessPhy set freq_ 2.42500e+09
Phy/WirelessPhy set L_ 4.61777e+03
Phy/WirelessPhy set CSThresh_ 1.00000e-10
Phy/WirelessPhy set RXThresh_ 6.10575e-10
#--------------------------------------
Propagation/Shadowing seed predef 42
# set up topography object
set topo       [new Topography]
$topo load_flatgrid $config(x) $config(y)

#set up channel object
set channel [ new $config(channel) ]

# Create God
set god_ [create-god $config(num_nodes)]

#ns configs
$ns_ node-config -adhocRouting $config(rp) \
		-llType $config(ll) \
		-macType $config(mac) \
		-ifqType $config(ifq) \
		-ifqLen $config(ifqlen) \
		-antType $config(ant) \
		-propType $config(prop) \
		-phyType $config(netif) \
		-topoInstance $topo \
		-agentTrace OFF \
		-routerTrace OFF \
		-phyTrace OFF \
		-macTrace OFF \
		-movementTrace OFF \
    -channel $channel

proc tcl_poller {} {
  while {1} { 
    source /tmp/poll.tcl
  }
}

proc createAgent { type nodeID channelNum { posX 0 } { posY 0 } { posZ 0 } } {
  global ns_ tracefd config agents nodes
  if {![ info exists nodes($nodeID)]} {
    set nodes($nodeID) [$ns_ node]
    $nodes($nodeID) set X_ $posX
    $nodes($nodeID) set Y_ $posY
    $nodes($nodeID) set Z_ $posZ
  }
  if {![ info exists agents(${nodeID}_${channelNum}) ]} { 
    set agents(${nodeID}_${channelNum}) [new Agent/$type]
    $ns_ attach-agent $nodes($nodeID) $agents(${nodeID}_${channelNum})
  }
  puts $tracefd "## [ $ns_ now ] add ${nodeID}_${channelNum} [$agents(${nodeID}_${channelNum}) set agent_addr_]:[$agents(${nodeID}_${channelNum}) set agent_port_]"
  flush $tracefd
}

Agent/UDP instproc process_data { uid data } {
  global ns_ tracefd agents nodes
  $self instvar agent_addr_ agent_port_ 
  puts $tracefd "## [ $ns_ now ] received $uid $agent_addr_:$agent_port_ $data"
  flush $tracefd
}

Node instproc num-agents { } {
	$self instvar agents_
  return [ llength agents_ ]
}

proc closeAgent { nodeID channelNum } {
  global ns_ agents nodes
  if {[ info exists agents(${nodeID}_${channelNum}) ]} { 
    $ns_ detach-agent $nodes($nodeID) $agents(${nodeID}_${channelNum}) 
    array unset agents ${nodeID}_${channelNum} 
  }
  if {![ $nodes($nodeID) num-agents ]} {
    array unset nodes $nodeID 
  }
}
proc newLocation { nodeID posX posY posZ } {
	global ns_ nodes
  if {[ info exists nodes($nodeID) ]} {
    $nodes($nodeID) set X_ $posX
    $nodes($nodeID) set Y_ $posY
    $nodes($nodeID) set Z_ $posZ
  }
}

proc scheduleNewLocation { baseTime nodeID posX posY posZ} {
	global ns_
	$ns_ at $baseTime "newLocation $nodeID $posX $posY $posZ"
}

proc sendUnicast { senderAID receiverAID packetSize args } {
  global ns_ tracefd agents
  if {[ info exists agents($senderAID) ] && [ info exists agents($receiverAID) ]} {
    $agents($senderAID) set dst_addr_ [$agents($receiverAID) set agent_addr_] 
    $agents($senderAID) set dst_port_ [$agents($receiverAID) set agent_port_]
    $agents($senderAID) send $packetSize $args 
    puts $tracefd "## [$ns_ now] unicast $senderAID to $receiverAID"
  }
}

proc scheduleUnicast { baseTime senderID receiverID channelNum packetSize args } {
	global ns_
	$ns_ at $baseTime "sendUnicast ${senderID}_${channelNum} ${receiverID}_${channelNum} $packetSize $args"
}

proc sendBroadcast { senderAID packetSize args } {
	global ns_ tracefd agents
  if {[info exists agents($senderAID)]} {
	  $agents($senderAID) set dst_addr_ -1 
	  $agents($senderAID) set dst_port_ [$agents($senderAID) set agent_port_] 
	  $agents($senderAID) send $packetSize $args 
    puts $tracefd "## [$ns_ now] broadcast $senderAID"
  }
}

proc scheduleBroadcast { baseTime senderID channelNum packetSize args } {
	global ns_ 
	$ns_ at $baseTime "sendBroadcast ${senderID}_${channelNum} $packetSize $args"
}

proc scheduleHaltThenResume { haltTime } {
	global ns_ namtrace tracefd
	$ns_ at $haltTime "$ns_ flush-trace"
	$ns_ at $haltTime "flush $namtrace"
	$ns_ at $haltTime "flush $tracefd"
	$ns_ at $haltTime "$ns_ halt"
  $ns_ resume
}
$ns_ at 0.5 "tcl_poller" 
$ns_ run
