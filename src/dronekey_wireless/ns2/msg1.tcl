global ns_ namtrace tracefd config agents nodes

#proc createAgent { type nodeID channelNum { posX 0 } { posY 0 } { posZ 0 } }
createAgent UDP UAV0 127 0 0 0
createAgent UDP UAV1 127 0 3 4 

#create more if you want ...
createAgent UDP UAV2 127 0 3 4 

#No need to worry about creating Nodes. bootscript takes care of that.
createAgent UDP UAV2 128 

#proc scheduleUnicast { baseTime senderID receiverID channelNum packetSize args }
scheduleUnicast 1.10 UAV0 UAV2 127 23 DummyString 

#proc scheduleHaltThenResume { haltTime }
scheduleHaltThenResume 1.15




#...some time later....

#proc scheduleNewLocation { baseTime nodeID posX posY posZ}
scheduleNewLocation 2.05 UAV2 0 4000 3000

#proc scheduleBroadcast { baseTime senderID channelNum packetSize args }
scheduleBroadcast 2.10 UAV0 127 24 A24BytePacket

#bootscript will help you ignore shit like this (undefined nodes):
scheduleUnicast 2.20 UAV3 UAV2 127 25 A25BytePacket 

#or this (undefined agents):
scheduleUnicast 2.25 UAV1 UAV2 [expr 127 + 1] 25 A25BytePacket 

scheduleHaltThenResume 2.50 




#...some time later....
#proc closeAgent { nodeID channelNum } {
#closeAgent UAV1 127 

scheduleNewLocation 2.75 UAV2 0 4 3

scheduleBroadcast 3.10 UAV2 127 26 A26BytePacket
scheduleBroadcast 3.20 UAV0 127 26 A26BytePacket

scheduleHaltThenResume 3.50 
