#ifndef DRONEKEY_NETWORK_SIMULATOR_HANDLER
#define DRONEKEY_NETWORK_SIMULATOR_HANDLER

#include <ros/ros.h>

#include <gazebo/physics/physics.hh>

//Boost include
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>

#include "dronekey_wireless/Packet.h"
#include <hal_quadrotor/GetTruth.h>

#include <iomanip>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <list>
#include <map>

typedef std::map<int, std::string> traceLine;

enum CMD_NS2 {
	GLOBAL,
	CREATE_AGENT,
	CLOSE_AGENT,
	SET_LOCATION,
	SCHEDULE_LOCATION,
	SEND_UNICAST,
	SCHEDULE_UNICAST,
	SEND_BROADCAST,
	SCHEDULE_BROADCAST,
	SCHEDULE_HALT
};

/**
 * State of a model
 */
struct model_state {
	std::string	name;
	float x, y, z;
};

/**
 * Structure to hold agent port and relative id
 */
struct ns_agent {
	std::string name;
	int port, portID;
};

/**
* Struct used in the queue to create simulation file
*/
//simulation_model changed to packet_state
struct packet_state {
	//UAV0_UAV1_90
	std::string pktID;
	float simTime;
	dronekey_wireless::Packet::Request payload;
	std::list<model_state> models;
	bool isSimulated;
};

namespace dronekey {
	class SimulationHandler {
	private:
		/**
		 * Flag that stops Simulation!!
		 */
		bool isStarted;

		/**
		 * Time interval at which buffer is dumped to NS
		 */
		static const int simulation_time_ms = 500;

		/**
		 * Simulation Thread
		 */
		boost::thread simThread;

		/**
		 * Path to Pipe and Trace File
		 */
		std::string pipePath, tracePath;

		/**
		 * Pipe File
		 */
		std::ofstream pipeFile;
	
		/**
		 * Trace File
		 */
		std::ifstream traceFile;

		/**
		 * Temportary Buffer for NS2 commands
		 */
		std::ostringstream bufferSS;

		/**
		 * Trekk the simulation time
		 */
		float simTime;

		/**
		 * World Reference
		 */
		gazebo::physics::WorldPtr &gWorld;

		boost::shared_ptr<ros::NodeHandle> &gRosNode;

		/**
		 * Store All wireless Models and corresponding NS IDs. If -1 not yeath added to NS!!
		 */
		std::map<std::string, int> wirelessModels;

		/**
		 * Keeps track of the node id in NS
		 */
		std::map<std::string, std::list<ns_agent> > nsInfo;

		/**
		 * Message buffer queue. Used so that simulation is more realistic
		 */
		std::list<packet_state> packetBuffer;

		/**
		 * Commit TCL code execution to poll.tcl
		 * @return bool isSuccess?
		 */
		bool commit	();

		/**
		 * Handles Commands that are available in NS2 TCL
		 * @param cmd  Command enumertator
		 * @param args Arguments required by command
		 */
		void cmd(CMD_NS2 cmd, std::string args);

		/**
		 * Transport the message from one model to another
		 * @return bool true if success
		 */
		bool msgTransport();

		/**
		 * Check whether a model in the world has a wireless module
		 * @param  serviceURI String of the model uri
		 * @return            Return true if has wireless module
		 */
		bool isWireless(std::string &modelName);

		/**
		 * Check whether all the packets passed have been send to NS2
		 * @return true if all is simulated
		 */
		bool isSimulationDone();

		/**
		 * - Dump the temporary buffer to NS2. 
		 * - Run trace
		 * - Delay buffer dumps
		 */
		void buffer2sim();

		/**
		 * Process the packets received and create the output required for TCL  NS2
		 */
		bool processPackets();

		void initBuffer();

		bool send2model(std::string &to, std::string &from, int port);

		void expandTrace(std::string &s, char delimiter, traceLine &result);

		void dumpTrace(traceLine &result);

		bool findPacket(std::string &from, std::string &to, std::string &pkt, std::list<packet_state>::iterator &iBuffer);

		int findPort(std::list<ns_agent> &agents, int portID);
	
		bool isAgentCreated(std::list<ns_agent> &agents, int port);
	public:
		/**
		 * Initialize Simulation handler. Open file for writing!!
		 */
		SimulationHandler(std::string fifoFile, std::string trFile, gazebo::physics::WorldPtr &world, boost::shared_ptr<ros::NodeHandle> &rosNode);

		~SimulationHandler();

		bool startSimulation();
	
		bool stopSimulation();

		bool appendPacket(dronekey_wireless::Packet::Request &req);

		void parseTrace();
	};
}	

#endif