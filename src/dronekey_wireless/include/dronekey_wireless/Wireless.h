#ifndef DRONEKEY_WIRELESS_H
#define DRONEKEY_WIRELESS_H

//Platform Includes
#include <ros/ros.h>
#include <ros/subscribe_options.h>
#include <ros/callback_queue.h>

//Handle NS2 Simulation
#include "dronekey_wireless/Packet.h"
#include <dronekey_wireless/SimulationHandler.h>

//Boost include
#include <boost/bind.hpp>
#include <string>
#include <list>


namespace dronekey {
	class Wireless {
	private:
		/**
		 * Ros Node Reference!
		 */
		boost::shared_ptr<ros::NodeHandle> &gRosNode;


		ros::CallbackQueue &gQueue;

		/**
		 * World Reference
		 */
		gazebo::physics::WorldPtr &gWorld;

		/**
		* Handle Simulator
		*/
		dronekey::SimulationHandler gSimulator;

		/**
		 * Advertiser Service
		 */
		ros::ServiceServer srvSend;

		/**
		 * When initializing object. Start Interface services.
		 *  - Connect
		 *  - Close
		 *  - Send
		 *  - SendTo
		 */
		void initServices();

		/**
		 * This function does not actually send the packet! First it is queued to be simulated. 
		 * Once simulated data is sent independently.
		 *
		 * @return If added successfully to the queue or not!
		 */
		bool send(dronekey_wireless::Packet::Request &req, dronekey_wireless::Packet::Response &res);

	public:
		/**
		 * Initialize Wireless Simulation
		 */
		void startWireless();

		/**
		 * Stop Wireless Simulation
		 */
		void stopWireless();

		/**
		 * Constructor
		 */
		Wireless(gazebo::physics::WorldPtr &world, boost::shared_ptr<ros::NodeHandle> &rosNode, ros::CallbackQueue &q);
	};
}

#endif