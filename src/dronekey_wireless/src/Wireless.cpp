#include <dronekey_wireless/Wireless.h>

using namespace dronekey;

Wireless::Wireless(gazebo::physics::WorldPtr &world, boost::shared_ptr<ros::NodeHandle> &rosNode, ros::CallbackQueue &q) 
	: gWorld(world), gRosNode(rosNode), gQueue(q), gSimulator("/tmp/poll.tcl", "/tmp/simple.tr", world, rosNode) {}

void Wireless::initServices()
{
	//TCP Send
	ros::AdvertiseServiceOptions adSend = ros::AdvertiseServiceOptions::create<dronekey_wireless::Packet>(
		"Wireless/Send", boost::bind(&Wireless::send,this,_1,_2), ros::VoidPtr(), &gQueue
	);
	srvSend = gRosNode->advertiseService(adSend);	
}

void Wireless::startWireless()
{
	//Start all ros services needed for wireless
	initServices();

	//Start Simulator
	gSimulator.startSimulation();
}

void Wireless::stopWireless()
{
	gSimulator.stopSimulation();
}

bool Wireless::send(dronekey_wireless::Packet::Request &req, dronekey_wireless::Packet::Response &res)
{
	ROS_INFO("PORT %d", req.port);
	res.isSent = gSimulator.appendPacket(req);
	return res.isSent;
}