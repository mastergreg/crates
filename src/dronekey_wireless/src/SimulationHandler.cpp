#include <dronekey_wireless/SimulationHandler.h>

using namespace dronekey;

SimulationHandler::~SimulationHandler()
{
	stopSimulation();
}

SimulationHandler::SimulationHandler(std::string fifoFile, std::string trFile, gazebo::physics::WorldPtr &world, boost::shared_ptr<ros::NodeHandle> &rosNode) : gWorld(world), gRosNode(rosNode)
{
	pipePath = fifoFile;
	tracePath = trFile;
	simTime = 2.0;
}

bool SimulationHandler::startSimulation()
{
	isStarted = true;
	//Start Buffer
	initBuffer();
	//Start Thread that handles buffer to Simulator
	simThread = boost::thread(&SimulationHandler::buffer2sim, this);
	simThread.detach();
}

bool SimulationHandler::stopSimulation()
{
	isStarted = false;
}

bool SimulationHandler::appendPacket(dronekey_wireless::Packet::Request &req)
{
	//get All World Models
	gazebo::physics::Model_V allModels = gWorld->GetModels();
	//Flag to keep track if queued success
	bool isQueued = false;
	//Packet State
	packet_state sModel;
	//Model State
	model_state tmpModel;
	//String keeps URI
	std::string serviceURI;
	//Args stream!!
	std::ostringstream tmpArgs;
	
	hal_quadrotor::GetTruth pkt;	
	
	//Parse all models and if not tracked add to list!!
	for(std::vector<gazebo::physics::ModelPtr>::iterator it = allModels.begin(); it != allModels.end(); it++)
	{
		//type Cast Model and Get Name
		gazebo::physics::Model &md = *(*it);
		std::string modelName = md.GetName();

		//if model is Wireless get location
		if(isWireless(modelName) == true){
			//Get Model Location!
			serviceURI = "/hal/"+modelName+"/GetTruth";						
			ros::ServiceClient srvTruth = gRosNode->serviceClient<hal_quadrotor::GetTruth>(serviceURI);
			if(!srvTruth.call(pkt)){
				continue;
			}

			tmpModel.x = static_cast<float>(pkt.response.state.x);
			tmpModel.y = static_cast<float>(pkt.response.state.y);
			tmpModel.z = static_cast<float>(pkt.response.state.z);
			tmpModel.name = modelName;
			
			//Not being tracked!! Add to map and simulation
			if(wirelessModels.find(modelName) == wirelessModels.end()){
				//Add to simulator
				tmpArgs.str("");
				tmpArgs.clear();
				tmpArgs.unsetf( std::ios::floatfield );
				tmpArgs << "UDP " << modelName << " " << req.port << " ";
				tmpArgs << std::ios::fixed << tmpModel.x << " " << tmpModel.y << " " << tmpModel.z;
				cmd(CREATE_AGENT, tmpArgs.str());

				//Add to MAP
				wirelessModels[modelName] = -1;
			}

			//Model Data
			isQueued = true;
			sModel.models.push_back(tmpModel);
		}

		ROS_INFO("Model Names: %s - Type: %d", md.GetName().c_str(), md.GetType());
	}
	
	//Set Model
	sModel.pktID = req.from+"_"+req.to+"_"+boost::lexical_cast<std::string>(req.port);
	sModel.payload = req;
	sModel.isSimulated = false;
	sModel.simTime = req.time;
	packetBuffer.push_back(sModel);
	return isQueued;
}

int SimulationHandler::findPort(std::list<ns_agent> &agents, int portID)
{
	for(std::list<ns_agent>::iterator iAgent = agents.begin(); iAgent != agents.end(); iAgent++){
		if(iAgent->portID == portID){
			return iAgent->port;
		}
	}
	return -1;
}

bool SimulationHandler::isAgentCreated(std::list<ns_agent> &agents, int port)
{
	for(std::list<ns_agent>::iterator iAgent = agents.begin(); iAgent != agents.end(); iAgent++){
		if(iAgent->port == port){
			return true;
		}
	}
	return false;
}

void SimulationHandler::parseTrace()
{
	std::string line;
	if(!traceFile.is_open()){
		traceFile.open(tracePath.c_str(), std::ofstream::in);
	}

	traceLine mapLine;
	std::string action;
	while(std::getline(traceFile, line)){
		//Expand trace line by space
		mapLine.clear();
		expandTrace(line, ' ', mapLine);
		dumpTrace(mapLine);
	
		if(mapLine[2].empty()){
			continue;
		}

		action = mapLine[2];
		if(action == "add"){
			ns_agent agentDetails;
			traceLine mapName, mapID;
			int nodeID;

			//Expand Name and IDs given by NS
			expandTrace(mapLine[3], '_', mapName);
			expandTrace(mapLine[4], ':', mapID);
			//Save Node if needed
			nodeID = atoi(mapID[0].c_str());
			if(wirelessModels[mapName[0]] == -1){
				wirelessModels[mapName[0]] = nodeID;
			}
			//Agent already saved?
			if(isAgentCreated(nsInfo[mapID[0]], atoi(mapName[1].c_str())) == true){
				continue;
			}

			ROS_INFO("NEW PORT ADDING!!");
			//Add port to agent
			agentDetails.port = atoi(mapName[1].c_str());
			agentDetails.portID = atoi(mapID[1].c_str());
			agentDetails.name = mapName[0];
			nsInfo[mapID[0]].push_back(agentDetails);

			ROS_INFO("ADD!!");
		} else if(action == "received"){
			std::list<ns_agent> agents;
			std::string nodeFrom, nodeTo;
			traceLine mapID;
			int port;

			agents = nsInfo[mapLine[3]];
			nodeFrom = agents.front().name;

			expandTrace(mapLine[4], ':', mapID);
			agents = nsInfo[mapID[0]];
			nodeTo = agents.front().name;
			port = findPort(agents, atoi(mapID[1].c_str()));
			//Success!! Send to model and remove from queue
			send2model(nodeFrom, nodeTo, port);

			ROS_INFO("RECEIVED!! %s", agents.front().name.c_str());
		}
	}
	traceFile.close();

	//RESET FILE!!
	std::ofstream ofs;
	ofs.open(tracePath.c_str(), std::ofstream::out | std::ofstream::trunc);
	ofs.close();
}

void SimulationHandler::cmd(CMD_NS2 cmd, std::string args)
{
	switch(cmd)
	{
		case GLOBAL:
			bufferSS << "global ns_ tracefd config agents nodes" << std::endl;
			break;
		case CREATE_AGENT:
			bufferSS << "createAgent " << args << std::endl;
			break;
		case CLOSE_AGENT:
			bufferSS << "closeAgent " << args << std::endl;
			break;
		case SET_LOCATION:
			bufferSS << "newLocation " << args << std::endl;
			break;
		case SCHEDULE_LOCATION:
			bufferSS << "scheduleNewLocation " << args << std::endl;
			break;
		case SEND_UNICAST:
			bufferSS << "sendUnicast " << args << std::endl;
			break;
		case SCHEDULE_UNICAST:
			bufferSS << "scheduleUnicast " << args << std::endl;
			break;
		case SEND_BROADCAST:
			bufferSS << "sendBroadcast " << args << std::endl;
			break;
		case SCHEDULE_BROADCAST:
			bufferSS << "scheduleBroadcast " << args << std::endl;
			break;
		case SCHEDULE_HALT:
			bufferSS << "scheduleHaltThenResume " << args << std::endl;
			break;
		default:
			break;
	}
}

bool SimulationHandler::commit()
{
	ROS_DEBUG("DRONEKEY_WIRELESS: Commiting to NS2!");

	//Check if file is already opened.
	if(!pipeFile.is_open()){
		//Open file
		pipeFile.open(pipePath.c_str(), std::ofstream::in);
	}

	bufferSS.seekp(0, std::ios::end);
	int ssSize = bufferSS.tellp();
	
  	// write to outfile
  	ROS_INFO("COMMITING!! %s", bufferSS.str().c_str());
  	pipeFile.write (bufferSS.str().c_str(), ssSize);
	pipeFile.close();

	//Buffer Commited clear!
	initBuffer();

	//Incremenet Simulation time so next run is infron of current time!
	simTime += 0.05;

	return true;
}

bool SimulationHandler::processPackets()
{
	bool isNewPacket = false;
	std::ostringstream tmpArgs;

	for(std::list<packet_state>::iterator iPackets = packetBuffer.begin(); iPackets != packetBuffer.end(); iPackets++){
		//Packet already simulated!
		if(iPackets->isSimulated){
			continue;
		}

		simTime += 0.05;
		isNewPacket = true;

		//Set Location to all Nodes!!
		for(std::list<model_state>::iterator iModel = iPackets->models.begin(); iModel != iPackets->models.end(); iModel++){
				tmpArgs.str("");
				tmpArgs.clear();
				//tmpArgs.precision(80);
				tmpArgs << simTime << " " << iModel->name << " ";
				tmpArgs << std::ios::fixed << iModel->x << " " << iModel->y << " " << iModel->z;
				cmd(SCHEDULE_LOCATION, tmpArgs.str());
		}

		iPackets->isSimulated =  true;
		iPackets->simTime = simTime;
		simTime += 0.05;

		tmpArgs.str("");
		tmpArgs.clear();
		tmpArgs << simTime << " " << iPackets->payload.from << " " << iPackets->payload.to << " " << iPackets->payload.port << " " << iPackets->payload.pkt.size() << " " << iPackets->payload.pkt;
		cmd(SCHEDULE_UNICAST, tmpArgs.str());
		
		simTime += 0.05;
	}

	tmpArgs.str("");
	tmpArgs.clear();
	tmpArgs << simTime;
	cmd(SCHEDULE_HALT, tmpArgs.str());
	simTime += 0.05;
	return isNewPacket;
}

void SimulationHandler::initBuffer()
{
	bufferSS.str("");
	bufferSS.clear();
	cmd(GLOBAL, "");
}

bool SimulationHandler::isWireless(std::string &modelName)
{
	return ros::service::exists("/hal/"+modelName+"/wireless/Receive", false);
}

bool SimulationHandler::isSimulationDone()
{
	for(std::list<packet_state>::iterator iPacket = packetBuffer.begin(); iPacket != packetBuffer.end(); iPacket++){
		if(!iPacket->isSimulated){
			return false;
		}
	}
	return true;
}

void SimulationHandler::dumpTrace(traceLine &result)
{
		ROS_INFO("-- DRONKEY TRACE?: %s -- TIME: %s -- TYPE: %s -- ", result[0].c_str(), result[1].c_str(), result[2].c_str());
}

bool SimulationHandler::findPacket(std::string &from, std::string &to, std::string &pkt, std::list<packet_state>::iterator &iBuffer)
{
	for(iBuffer = packetBuffer.begin(); iBuffer != packetBuffer.end(); iBuffer++){
		if(iBuffer->payload.to == to && iBuffer->payload.from == from && iBuffer->payload.pkt == pkt){
			return true;
		}
	}
	return false;
}

void SimulationHandler::expandTrace(std::string &s, char delimiter, traceLine &result)
{
	size_t pos = 0;
	std::string token;
	int index = 0;
	while ((pos = s.find(delimiter)) != std::string::npos) {
    	token = s.substr(0, pos);
    	result[index] = token;
    	s.erase(0, pos + 1);
    	index += 1;
	}
	result[index] = s;
}

void SimulationHandler::buffer2sim()
{
	while(1){
		//Stop Simulation
		if(!isStarted){
			break;
		}

		//Sleep the thread for stipulated time
		boost::this_thread::sleep(boost::posix_time::milliseconds(simulation_time_ms));
		
		//@todo make this in less interval!
		//Collect Trace!!
		parseTrace();
		gWorld->SetPaused(false);

		//Packet buffer empty!
		if(packetBuffer.empty() || isSimulationDone()){
			continue;
		}

		ROS_INFO("BUFFER IS WAITING %s", bufferSS.str().c_str());
		//Pause the world
		gWorld->SetPaused(true);

		//Read the packet buffer and create corresponding TCL Script
		if(processPackets()) {
			commit();
		}
	}
}

bool SimulationHandler::send2model(std::string &to, std::string &from, int port)
{
	ROS_INFO("SEND 2 MODEL!! %s-%s:%d",to.c_str(), from.c_str(), port);
	std::string serviceURI;
	std::string pktID = to+"_"+from+"_"+boost::lexical_cast<std::string>(port);
	for(std::list<packet_state>::iterator iPacket = packetBuffer.begin(); iPacket != packetBuffer.end();){
		if(pktID != iPacket->pktID){
			iPacket++;
			continue;
		}
		
		dronekey_wireless::Packet newPkt;
		newPkt.request = iPacket->payload;
		
		serviceURI = "/hal/"+to+"/wireless/Receive";
		ros::ServiceClient srvWireless = gRosNode->serviceClient<dronekey_wireless::Packet>(serviceURI);
		if(!srvWireless.call(newPkt)){
				ROS_FATAL("Packet Receiver!!");
		}

		//Sent to Model so delete
		packetBuffer.erase(iPacket++);
	}
	return true;
}